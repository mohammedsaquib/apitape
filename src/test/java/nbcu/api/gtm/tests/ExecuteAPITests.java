package nbcu.api.gtm.tests;

import java.awt.AWTException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.birlasoft.base.BaseTest;
import com.birlasoft.keywords.KeywordDriver;
import com.birlasoft.libs.Constants;
import com.birlasoft.libs.data.DataUtil;
import com.birlasoft.libs.data.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class ExecuteAPITests extends BaseTest {
	@BeforeTest
	public void init() {
		xls = new Xls_Reader(Constants.NetAPI_XLS);
	}

	@Test(dataProvider = "getData")
	public void createAPITests(List<Hashtable<String, String>> data) throws InterruptedException, AWTException {
		List<Hashtable<String, String>> testcase = (List<Hashtable<String, String>>) data.iterator().next();
		for (int j = 0; j < testcase.size(); j++)
		{
			Hashtable<?, ?> htable = (Hashtable<?, ?>) testcase.get(j);
			Enumeration<?> keys = htable.keys();

			String testcases = keys.nextElement().toString();
			test = rep.startTest(testcases);
			test.log(LogStatus.INFO, data.toString());
			
			System.out.println("############# Starting ["+ test.getTest().getName() +"] Scenario  ######################");
			System.out.println();
			System.out.println();
			
			if (DataUtil.isSkip(xls, testcases)) {
				test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
				throw new SkipException("Skipping the test as runmode is N");
			}

			test.log(LogStatus.INFO, "Starting " + testcases);

			app = new KeywordDriver(test);

			test.log(LogStatus.INFO, "Executing keywords");
			try {
				app.executeKeywords(testcases, xls, (Hashtable<String, String>) htable.get(testcases));
				test.log(LogStatus.PASS, "PASS");
			} catch (Exception e) {
				test.log(LogStatus.FAIL,e.getMessage(), "Fail");
			}
			rep.flush();	
		}
	}
}
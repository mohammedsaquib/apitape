package com.birlasoft.keywords;

import java.awt.AWTException;
import java.util.Hashtable;

import org.junit.Assert;

import com.birlasoft.libs.Constants;
import com.birlasoft.libs.data.Xls_Reader;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class KeywordDriver {
	ExtentTest test;
	AppKeywords app;
	APIKeywords api;

	public KeywordDriver(ExtentTest test) {
		this.test = test;
	}

	public void executeKeywords(String testUnderExecution, Xls_Reader xls, Hashtable<String, String> testData)
			throws InterruptedException, AWTException {
		app = new AppKeywords(test);
		api = new APIKeywords(test);

		int rows = xls.getRowCount(Constants.KEYWORDS_SHEET);

		for (int rNum = 2; rNum <= rows; rNum++) {
			String tcid = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.TCID_COL, rNum);

			if (tcid.equals(testUnderExecution)) {
				String keyword = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.KEYWORD_COL, rNum);
				String object = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.OBJECT_COL, rNum);
				String key = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.DATA_COL, rNum);
				String data = testData.get(key);
				test.log(LogStatus.INFO, tcid + " -- " + keyword + " -- " + object + " --- " + data);

				System.out.println("Keyword >>" + keyword + " *****Object >>" + object + " ***** Data >>" + data);

				String result = "";
				if (keyword.equals("openBrowser"))
					result = app.openBrowser(data);
				else if (keyword.equals("navigate"))
					result = app.navigate(object);
				else if (keyword.equals("closeBrowser"))
					result = app.closeBrowser();
				else if (keyword.equals("click"))
					result = app.click(object);
				else if (keyword.equals("clear"))
					result = app.clear(object);
				else if (keyword.equals("alterOK"))
					result = app.alterOK();
				else if (keyword.equals("input"))
					result = app.input(object, data);
				else if (keyword.equals("verifyText"))
					result = app.verifyText(object, data);
				else if (keyword.equals("verifyElementPresent"))
					result = app.verifyElementPresent(object);
				else if (keyword.equals("wait"))
					result = app.wait(object);
				else if (keyword.equals("scrollTo"))
					result = app.scrollTo(object);
				else if (keyword.equals("PostAPI"))
					result = api.upsertPerson(data);
				else if (keyword.equals("setBaseURI"))
					result = api.setBaseURI(data);
				else if (keyword.equals("GetAPIData"))
					result = api.getAPIData();
				else if (keyword.equals("SearchDataAPI"))
					result = api.getPersonAPI(data);
				else if (keyword.equals("SerachResponse"))
					result = api.getPersonResponse(data);
				else if (keyword.equals("verifyAPISearchResult"))
					result = api.verifyGetPersonAPI(data);
				else if (keyword.equals("verifyBadData"))
					result = api.verifyBadData(data);
				else if (keyword.equals("verifyDuplicateData"))
					result = api.verifyDuplicateData(data);
				else if (keyword.equals("getAPIDataUsingCookie")) {
					result = api.getAPIDataUsingCookie(app.getBrowserCookies(), data,
							testData.get("ExpectedOutputJson"));
				} else if (keyword.equals("getDualAPIDataUsingCookie")) {
					result = api.getDualAPIDataUsingCookie(app.getBrowserCookies(), data,
							testData.get("ExpectedOutputJson"));
				} else if (keyword.equals("getFirstThenPostAPIDataUsingCookie")) {
					result = api.getFirstThenPostAPIDataUsingCookie(app.getBrowserCookies(), data,"series",testData.get("ExpectedOutputJson"));
				} else if (keyword.equals("postAPIDataUsingCookie")) {
					result = api.postAPIDataUsingCookie(app.getBrowserCookies(), data,
							testData.get("ExpectedOutputJson"));
				} else if (keyword.equals("authenticateUser")) {
					result = app.openBrowser(app.prop.getProperty("browserType"));
					result = app.navigate("gtmURL");
					result = app.input("gtm_username", app.prop.getProperty("gtmUser"));
					result = app.input("gtm_password", app.prop.getProperty("gtmPassword"));
					result = app.click("gtm_submit");
				}
				if (!result.equals(Constants.PASS)) {
					app.reportFailure(result);
					api.reportFailure(result);
					// Assert.fail(result);
				}
			}
		}
	}

	public AppKeywords getGenericKeyWords() {
		return app;
	}
}
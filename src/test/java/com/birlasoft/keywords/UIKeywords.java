package com.birlasoft.keywords;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.birlasoft.libs.Constants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class UIKeywords {
	public WebDriver driver;
	public Properties prop;
	ExtentTest test;
	public Alert alert;
	private static String browserCookies = null;

	public UIKeywords(ExtentTest test) {
		this.test = test;

		prop = new Properties();

		try {
			FileInputStream fs = new FileInputStream(
					System.getProperty("user.dir") + "//src//test//resources//project.properties");
			prop.load(fs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String openBrowser(String browserType) {
		test.log(LogStatus.INFO, "Opening Browser of type " + browserType);

		try {
			if (prop.getProperty("grid").equals("Y")) {
				DesiredCapabilities cap = null;

				if (browserType.equals("Mozilla")) {
					cap = DesiredCapabilities.firefox();
					cap.setBrowserName("firefox");
					cap.setJavascriptEnabled(true);
					cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				} else if (browserType.equals("Chrome")) {
					cap = DesiredCapabilities.chrome();
					cap.setBrowserName("chrome");
					cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				}

				driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
			} else {
				if (browserType.equals("Mozilla")) {
					driver = new FirefoxDriver();
				} else if (browserType.equals("Chrome")) {
					System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
					driver = new ChromeDriver();
				} else if (browserType.equals("IE")) {
					DesiredCapabilities caps = DesiredCapabilities.internetExplorer();

					caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
					caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
					caps.setCapability("ignoreProtectedModeSettings", true);
					caps.setCapability("nativeEvents", false);
					caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					caps.setBrowserName(BrowserType.IE);

					System.setProperty("webdriver.ie.driver",
							new File("drivers\\IEDriverServer.exe").getAbsolutePath());
					driver = new InternetExplorerDriver(caps);
				}
			}

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();

			test.log(LogStatus.PASS, "Sucessfully opened browser");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName() + " occured, while opening browser");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String navigate(String urlKey) {
		test.log(LogStatus.INFO, "Navigating to " + prop.getProperty(urlKey));

		try {
			driver.get(prop.getProperty(urlKey));
			test.log(LogStatus.PASS, "Sucessfully navigated to URL " + urlKey);
		} catch (Exception e) {
			test.log(LogStatus.FAIL,
					"Exception " + e.getClass().getName() + " occured, while navigating to URL " + urlKey);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String click(String locatorKey) {
		test.log(LogStatus.INFO, "Clicking on " + prop.getProperty(locatorKey));

		try {
			driver.findElement(By.xpath(prop.getProperty(locatorKey))).click();
			test.log(LogStatus.PASS, "Sucessfully clicked on object located by " + locatorKey);
			setBrowserCookies();
		} catch (Exception e) {
			setBrowserCookies();
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName()
					+ " occured, while clicking on object located by " + locatorKey);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String clear(String locatorKey) {
		test.log(LogStatus.INFO, "Clicking on " + prop.getProperty(locatorKey));

		try {
			driver.findElement(By.xpath(prop.getProperty(locatorKey))).clear();
			test.log(LogStatus.PASS, "Sucessfully clear element located by " + locatorKey);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName()
					+ " occured, while clearing element located by " + locatorKey);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String alterOK() {
		test.log(LogStatus.INFO, "Alert OK");

		try {
			alert = driver.switchTo().alert();
			alert.accept();
			test.log(LogStatus.PASS, "Sucessfully accepted alert");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName() + " occured, while accepting alert");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String input(String locatorKey, String data) {
		test.log(LogStatus.INFO, "Typing in data " + data + " element located by " + locatorKey);

		try {
			driver.findElement(By.xpath(prop.getProperty(locatorKey))).sendKeys(data);
			test.log(LogStatus.PASS,
					"Typing in data " + data + " in element located by " + prop.getProperty(locatorKey));
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName() + " occured, while typing in data " + data
					+ " in element located by " + locatorKey);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String closeBrowser() {
		test.log(LogStatus.INFO, "Closing browser");

		try {
			if(null != driver) {
				driver.close();
				driver.quit();
			}
			test.log(LogStatus.PASS, "Step", "Successfully closed browser");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName() + " occured, while closing browser");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	/***************************
	 * Validation keywords
	 *********************************/
	public String verifyText(String locatorKey, String expectedText) {
		test.log(LogStatus.INFO, "Verifying Text in element located by " + locatorKey);

		try {
			String actualText = driver.findElement(By.xpath(prop.getProperty(locatorKey))).getText();

			test.log(LogStatus.INFO, "Locator is " + prop.getProperty(locatorKey));
			test.log(LogStatus.INFO, "Actual--" + actualText, "Expexted--" + expectedText);

			if (actualText.equalsIgnoreCase(expectedText)) {
				test.log(LogStatus.PASS, "Successfully verified text. Expected Text " + expectedText
						+ " is equal to Actual text " + actualText);
			} else {
				test.log(LogStatus.FAIL, "Failed to verify text. Expected Text " + expectedText
						+ " is not equal to Actual text " + actualText);
				return Constants.FAIL;
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName()
					+ " occured, while verifying data of element located by " + locatorKey);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}
		return Constants.PASS;
	}

	public String verifyElementPresent(String locatorKey) {
		test.log(LogStatus.INFO, "Verifying existence of element located by " + locatorKey);

		try {
			if (!isElementPresent(locatorKey)) {
				test.log(LogStatus.FAIL, "Failed to verify existence of element located by " + locatorKey);
				return Constants.FAIL + " - Could not find Element " + locatorKey;
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName()
					+ " occured, while verifying element located by " + locatorKey);
			return Constants.FAIL + " - Exception " + e.getClass().getName() + " occured. Could not find Element "
					+ locatorKey;
		}
		
		test.log(LogStatus.PASS, "Successfully verified existence of element located by " + locatorKey);
		return Constants.PASS;
	}

	public String verifyElementNotPresent(String locatorKey) {
		test.log(LogStatus.INFO, "Verifying non-existence of element located by " + locatorKey);

		try {
			if (isElementPresent(locatorKey)) {
				test.log(LogStatus.FAIL, "Failed to verify non-existence of element located by " + locatorKey);
				return Constants.FAIL + " - Could not find Element " + locatorKey;
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Exception " + e.getClass().getName()
					+ " occured, while verifying non-existence element located by " + locatorKey);
			return Constants.FAIL + " - Exception " + e.getClass().getName()
					+ " occured. Could not find Element located by " + locatorKey;
		}
		
		test.log(LogStatus.PASS, "Successfully verified non-existence of element located by " + locatorKey);
		return Constants.PASS;
	}

	public String scrollTo(String xpathKey) {
		test.log(LogStatus.INFO, "Scrolling to element located by " + xpathKey);

		try {
			int y = getElement(xpathKey).getLocation().y;
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0," + (y - 200) + ")");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Failed to scroll element located by " + xpathKey);
			return Constants.FAIL + " - Exception " + e.getClass().getName()
					+ " occured. Could not scroll to Element located by " + xpathKey;
		}
		
		test.log(LogStatus.PASS, "Successfully scrolling element located by " + xpathKey);
		return Constants.PASS;
	}
	
	public void setBrowserCookies() {
		String cookies = "";
		Set<Cookie> _setValues = driver.manage().getCookies();
        for (Cookie value : _setValues) {
               cookies = cookies == null ? value.getName().toString() + "=" + value.getValue().toString()
                            : cookies + ";" + value.getName().toString() + "=" + value.getValue().toString();
        }
        browserCookies = cookies;
	}
	
	public String getBrowserCookies() {
		return browserCookies;
	}

	public String wait(String timeout) {
		try {
			long time = Integer.parseInt(prop.getProperty(timeout));
			Thread.sleep(time);
		} catch (Exception e) {
			test.log(LogStatus.WARNING, "Exception " + e.getMessage() + " occured while waiting");
		}
		return Constants.PASS;
	}

	/************************
	 * Utility Functions
	 ********************************/
	public WebElement getElement(String locatorKey) {
		WebElement e = null;
		try {
			if (locatorKey.endsWith("_id"))
				e = driver.findElement(By.id(prop.getProperty(locatorKey)));
			else if (locatorKey.endsWith("_xpath"))
				e = driver.findElement(By.xpath(prop.getProperty(locatorKey)));
			else if (locatorKey.endsWith("_name"))
				e = driver.findElement(By.name(prop.getProperty(locatorKey)));
		} catch (Exception ex) {
			reportFailure("Failure in Element Extraction - " + locatorKey);
			Assert.fail("Failure in Element Extraction - " + locatorKey);
		}
		return e;
	}

	public boolean isElementPresent(String locatorKey) {
		List<WebElement> e = null;

		e = driver.findElements(By.xpath(prop.getProperty(locatorKey)));

		if (e.size() == 0)
			return false;
		else
			return true;
	}

	/******************************
	 * Reporting functions
	 ******************************/

	public void reportFailure(String failureMessage) {
		takeScreenShot();
		test.log(LogStatus.FAIL, failureMessage);
	}

	public void takeScreenShot() {
		// decide name - time stamp
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_") + ".png";
		String path = null;

		try {
			path = new File(Constants.SCREENSHOT_PATH + screenshotFile).getCanonicalPath();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// take screenshot
		if(null != driver) {
			File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(srcFile, new File(path));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Embed
		test.log(LogStatus.INFO, test.addScreenCapture(path));
	}
}
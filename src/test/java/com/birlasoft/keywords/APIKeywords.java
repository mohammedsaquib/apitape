package com.birlasoft.keywords;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.json.JSONObject;

import com.birlasoft.libs.Constants;
import com.birlasoft.libs.api.RestServiceUtils;
import com.birlasoft.validator.JsonParser;
import com.birlasoft.validator.JsonValidator;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.restassured.response.Response;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class APIKeywords {
	public Properties prop;
	ExtentTest test;
	public String response;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

	public APIKeywords(ExtentTest test) {
		this.test = test;

		prop = new Properties();

		try {
			FileInputStream fs = new FileInputStream(
					System.getProperty("user.dir") + "//src//test//resources//project.properties");
			prop.load(fs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String setBaseURI(String data) {
		test.log(LogStatus.INFO, "setBaseURI" + data);

		try {
			RestServiceUtils.setBaseURI(data);
			test.log(LogStatus.PASS, "Successfully setting base URI " + data);
		} catch (Exception e) {
			test.log(LogStatus.FAIL,
					". Exception " + e.getClass().getName() + " occured. Failed to set base URI " + data);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String upsertPerson(String data) {
		test.log(LogStatus.INFO, "Create/Add/post record API");

		try {
			if (!RestServiceUtils.checkHttp_post("/Post", data)) {
				test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of POST request for payload " + data);
				return Constants.FAIL + ". Error / Bad / Unknown Response";
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of POST request for payload " + data);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		test.log(LogStatus.PASS, "Pass. Response of POST request is OK for payload " + data);
		return Constants.PASS;
	}

	public String getAPIData() {
		test.log(LogStatus.INFO, "Get API data ");

		try {
			String response = RestServiceUtils.http_get("/get");
			JsonArray jsonArray = RestServiceUtils.getJSonResponse(response);
			test.log(LogStatus.INFO, "Get API data--" + response);
			test.log(LogStatus.INFO, "First Data--" + jsonArray.get(0));
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of GET request");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	// Written only for POC - not a genric one
	public String getDualAPIDataUsingCookie(String cookie, String inputDataJson, String expectedOutputJson) {
		test.log(LogStatus.INFO, "API data");
		String endPoint = "";
		JsonArray inputData = null;
		Response responseObj = null;
		Map<String, String> _response = null;
		Map<String, String> _returnParamOutput = new HashMap<String, String>();
		String id = null;

		Map<String, String> headers = new HashMap<String, String>();
		List<String> inputDataList = new ArrayList<String>();

		if (!inputDataJson.contains("~")) {
			inputDataList.add(inputDataJson);
		} else {
			inputDataList = Arrays.asList(inputDataJson.split("\\~"));
		}

		for (int i = 0; i < inputDataList.size(); i++) {
			List<String> params = new ArrayList<String>();
			if (i > 0 & _response != null) {
				if (inputDataList.get(i).toString().contains("?")) {
					if (inputDataList.get(i).toString().contains("&"))
						params = Arrays.asList(inputDataList.get(i).toString().replace("}", "")
								.replace("" + (char) 34, "").trim().split("\\?")[1].split("&"));
					params = Arrays.asList(inputDataList.get(i).toString().replace("}", "").replace("" + (char) 34, "")
							.trim().split("\\?")[1]);

					JsonArray jsonArray = RestServiceUtils.getJSonResponse("[" + _response.toString() + "]");
					JsonObject jsonObject = jsonArray.get(0).getAsJsonObject().get("docs").getAsJsonObject();

					for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
						String key = entry.getKey();
						JsonObject jsonInnerObject = jsonObject.get(key).getAsJsonObject();
						for (Map.Entry<String, JsonElement> nestedEntry : jsonInnerObject.entrySet()) {
							String nestedKey = nestedEntry.getKey();
							if (nestedKey.equalsIgnoreCase("sourceTitleId") && params.indexOf("titleId_s") > -1) { // need_to_work_here...
								id = id == null ? nestedEntry.getValue().getAsString()
										: id + "," + nestedEntry.getValue().getAsString();
								_returnParamOutput.put("titleId_s", id);
								break;
							}
						}
					}
				}
			}

			List<JsonArray> listInputArray = new ArrayList<JsonArray>();
			List<String> list = null;
			if (_returnParamOutput.size() > 0) {
				inputData = RestServiceUtils.getJSonResponse("[" + inputDataList.get(i).toString().replace("titleId_s",
						"titleId_s=" + _returnParamOutput.get("titleId_s").split(",")[0]) + "]");
				list = new ArrayList<String>(Arrays.asList(_returnParamOutput.get("titleId_s").split(",")));
				for (int cntr = 0; cntr < list.size(); cntr++) {
					inputData = RestServiceUtils.getJSonResponse("[" + inputDataList.get(i).toString()
							.replace("titleId_s", "titleId_s=" + list.get(cntr).toString()) + "]");
					listInputArray.add(inputData);
				}
			} else {

				inputData = RestServiceUtils.getJSonResponse("[" + inputDataList.get(i).toString() + "]");
				listInputArray.add(inputData);
			}

			// test.log(LogStatus.INFO, "Total " + listInputArray.size() + " record(s) are
			// found");

			for (int jCnt = 0; jCnt < listInputArray.size(); jCnt++) {
				JsonObject elements = listInputArray.get(jCnt).get(0).getAsJsonObject();

				for (Map.Entry<String, JsonElement> entry : elements.entrySet()) {
					String key = entry.getKey();
					if (key.equalsIgnoreCase("EndPoint")) {
						endPoint = entry.getValue().getAsString();
					}
				}
				try {
					responseObj = RestServiceUtils.http_getUsingCookies(headers, cookie, "application/json", endPoint);
					_response = JsonValidator.jsonParser(responseObj.asString());

					Map<String, Object> map = JsonValidator.jsonToMap(responseObj.getBody().asString());

					long elapsedTime = responseObj.getTime() / 1000;
					test.log(LogStatus.INFO, "API Response Code: " + responseObj.getStatusCode() + ", Time Elapsed: "
							+ elapsedTime + " seconds");

					writeFile(test.getTest().getName() + dateFormat.format(new Date()) + ".txt",
							new StringBuffer(responseObj.asString()));

					if (i == inputDataList.size() - 1) { // earlier it was >1
						validateResponseThroughDB(map, expectedOutputJson, list.get(jCnt),"");
					}

				} catch (Exception e) {
					test.log(LogStatus.FAIL,
							". Error / Bad  Unknown Response of Get request or Exception" + e.getMessage());
					return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
				}
			}
		}
		return Constants.PASS; // need to work on return type

	}

	private String validateResponseThroughDB(Map<String, Object> map, String queryKey, String parameters,String comment) {
		String[] temp = null;
		if (queryKey.equalsIgnoreCase(Constants.SEARCH_QUERY_BY_Title_Name)
				|| queryKey.equalsIgnoreCase(Constants.SEARCH_QUERY_BY_Series_Name2)
				|| queryKey.equalsIgnoreCase(Constants.SEARCH_QUERY_BY_Series_Name1)) {
			int count = prop.getProperty(queryKey).length() - prop.getProperty(queryKey).replace("?", "").length();
			temp = new String[count];
			for (int i = 0; i < count; i++) {
				temp[i] = parameters;
			}
		}

		Map<Integer, HashMap> dbMap = JsonValidator.getDBDetails(queryKey,test,temp);

		if (parameters != null) {
			if(comment==null)
			test.log(LogStatus.INFO, "<b><font color=\"green\">Verifying API Response Details For GTM ID# " + parameters+ "</font><bold>");
			else
				test.log(LogStatus.INFO, "<b><font color=\"green\">Verifying API Response Details For GTM ID# ("+ comment+") "+ parameters+ "</font><bold>");
		}
		boolean flag = true;
		HashMap hMap = null;
		for (Entry<Integer, HashMap> entry : dbMap.entrySet()) {
			int key = entry.getKey();
			HashMap<String, String> dbResults = dbMap.get(key);
			try {
				hMap = (HashMap) (map.get("" + key));

			} catch (Exception e) {

				System.out.println(e.getLocalizedMessage());
			}
			test.log(LogStatus.INFO, "Valdating API Response From With Database");
			test.log(LogStatus.INFO,
					"#################################################################################");

			StringBuilder builder = createTableLayout();
			String startRowCSS = "<tr>";
			String startTDCSS = "<td class=\"tg-3fs7\">";
			String startFirstTDCSS = "<td class=\"tg-3fs7\" width=\"20%\">";
			String endTDCSS = "</td>";
			String endRow = "</tr>";
			String closeTable = "</table>";

			for (Entry<String, String> childEntry : dbResults.entrySet()) {
				String keys = childEntry.getKey();

				String responseValue = null;

				String databaseValue = dbResults.get(keys).replace("" + (char) 34, "").trim();
				databaseValue = databaseValue.replace("&amp;", "&").trim();
				try {
					if (hMap == null)
						responseValue = map.get(keys).toString().replace("[", "").replace("]", "")
								.replace("" + (char) 34, "").replace("\\", "").trim();
					else
						responseValue = hMap.get(keys).toString().replace("[", "").replace("]", "")
								.replace("" + (char) 34, "").replace("\\", "").trim();
				} catch (Exception ex) {
					responseValue = "none";
				}
				if (keys.equalsIgnoreCase("seasonYear"))
					responseValue = responseValue.replace("/", "").trim();
				if (databaseValue.startsWith(","))
					databaseValue = databaseValue.substring(1);
				if (databaseValue.endsWith(","))
					databaseValue = databaseValue.substring(0, databaseValue.length() - 1);

				if (databaseValue.toLowerCase().trim().contains(responseValue.toLowerCase().trim())) {
					// test.log(LogStatus.PASS,keys +"| "+ responseValue + " | " + databaseValue +
					// "");
					builder.append(
							startRowCSS + startFirstTDCSS + keys + endTDCSS + startFirstTDCSS + responseValue + endTDCSS
									+ startTDCSS + databaseValue + endTDCSS + startTDCSS + "<b><font color=\"green\">"
									+ LogStatus.PASS.toString().toUpperCase() + "</font></b>" + endTDCSS + endRow);

				} else if (databaseValue.contains(",") && responseValue.contains(",")) {
					List<String> dbList = Arrays
							.asList((Arrays.stream(databaseValue.split(",")).map(String::trim).toArray(String[]::new)));
					Collections.sort(dbList);
					List<String> resList = Arrays
							.asList(Arrays.stream(responseValue.split(",")).map(String::trim).toArray(String[]::new));
					Collections.sort(resList);
					if (resList.equals(dbList)) {
						// test.log(LogStatus.PASS,keys +"| "+ resList.toString() + " | " +
						// dbList.toString() + "");
						builder.append(startRowCSS + startFirstTDCSS + keys + endTDCSS + startFirstTDCSS
								+ resList.toString() + endTDCSS + startTDCSS + dbList.toString() + endTDCSS + startTDCSS
								+ "<b><font color=\"green\">" + LogStatus.PASS.toString().toUpperCase() + "</font></b>"
								+ endTDCSS + endRow);

					} else {
						// test.log(LogStatus.FAIL,keys +"| "+ resList.toString() + " | " +
						// dbList.toString() + "");
						builder.append(startRowCSS + startFirstTDCSS + keys + endTDCSS + startFirstTDCSS
								+ resList.toString() + endTDCSS + startTDCSS + dbList.toString() + endTDCSS + startTDCSS
								+ "<b><font color=\"red\">" + LogStatus.FAIL.toString().toUpperCase() + "</font></b>"
								+ endTDCSS + endRow);
						flag = false;
					}
				} else {
					builder.append(startRowCSS + startFirstTDCSS + keys + endTDCSS + startFirstTDCSS + responseValue
							+ endTDCSS + startTDCSS + databaseValue + endTDCSS + startTDCSS + "<b><font color=\"red\">"
							+ LogStatus.FAIL.toString().toUpperCase() + "</font></b>" + endTDCSS + endRow);
					flag = false;
				}

			}
			builder.append(closeTable);
			if (flag)
				test.log(LogStatus.PASS, builder.toString());
			else
				test.log(LogStatus.FAIL, builder.toString());
			test.log(LogStatus.INFO, "Valdation Completed");
			test.log(LogStatus.INFO,
					"#################################################################################");

		}
		return Constants.PASS;
	}

	private StringBuilder createTableLayout() {
		String startTHCSS = "<th class=\"tg-3fs7\">";
		String endTHCSS = "</th>";

		StringBuilder sb = new StringBuilder();
		sb.append("<style type=\"text/css\">\r\n"
				+ ".tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}\r\n"
				+ ".tg td{width:25%;font-family:Calibri;font-size:14px;padding:15px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}\r\n"
				+ ".tg th{font-family:Calibri;font-size:14px;font-weight:bold;padding:15px 5px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}\r\n"
				+ ".tg .tg-3fs7{font-size:11px;font-family:Tahoma, Geneva, sans-serif !important;;vertical-align:top}\r\n"
				+ ".tg .tg-yw4l{vertical-align:top}\r\n" + "</style>" + "<table class=\"tg\" style=\"width\"=\"100%\">"
				+ startTHCSS + "Field Value" + endTHCSS + startTHCSS + "Respone Value" + endTHCSS + startTHCSS
				+ "Database Value" + endTHCSS + startTHCSS + "Status" + endTHCSS);
		return sb;
	}

	public String getAPIDataUsingCookie(String cookie, String inputDataJson, String expectedOutputJson) {
		test.log(LogStatus.INFO, "Get API data ");
		Map<String, String> headers = new HashMap<String, String>();
		JsonArray inputData = RestServiceUtils.getJSonResponse("[" + inputDataJson + "]");
		String endPoint = "";
		JsonObject elements = inputData.get(0).getAsJsonObject();
		for (Map.Entry<String, JsonElement> entry : elements.entrySet()) {
			String key = entry.getKey();
			if (key.equalsIgnoreCase("EndPoint")) {
				endPoint = entry.getValue().getAsString();
			}
		}
		Response responseObj = null;
		try {
			responseObj = RestServiceUtils.http_getUsingCookies(headers, cookie, "application/json", endPoint);
			long elapsedTime = responseObj.getTime() / 1000;
			test.log(LogStatus.INFO, "Get API Response Code: " + responseObj.getStatusCode() + ", Time Elapsed: "
					+ elapsedTime + " seconds");
			writeFile(test.getTest().getName() + dateFormat.format(new Date()) + ".txt",
					new StringBuffer(responseObj.asString()));
			if (expectedOutputJson.isEmpty()) {
				return Constants.PASS;
			}
			return validateResponse(responseObj.getBody().asString(), expectedOutputJson);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Get request");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}
	}

	private void writeFile(String fileName, StringBuffer content) {
		BufferedWriter bw = null;
		try {
			File dir = new File(System.getProperty("user.dir") + "/response/");
			dir.mkdirs();
			FileWriter fw = new FileWriter(dir.getAbsolutePath() + File.separator + fileName);
			bw = new BufferedWriter(fw);
			System.out.println("Writing content of length " + content.length() + " in " + fileName);
			bw.write(content.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
			} catch (IOException e) {
			}
		}
	}

	public String postAPIDataUsingCookie(String cookie, String inputDataJson, String expectedOutputJson) {
		test.log(LogStatus.INFO, "Post API data ");
		Map<String, String> headers = new HashMap<String, String>();
		JsonArray inputDataJsonArray = RestServiceUtils.getJSonResponse("[" + inputDataJson + "]");
		String endPoint = "", body = " ";
		JsonObject elements = inputDataJsonArray.get(0).getAsJsonObject();
		for (Map.Entry<String, JsonElement> entry : elements.entrySet()) {
			String key = entry.getKey();
			if (key.equalsIgnoreCase("EndPoint")) {
				endPoint = entry.getValue().getAsString();
			} else if (key.equalsIgnoreCase("Body")) {
				body = entry.getValue().getAsString();
			}
		}
		Response responseObj = null;
		try {
			responseObj = RestServiceUtils.http_postUsingCookies(headers, cookie, "application/json", endPoint, body);
			long elapsedTime = responseObj.getTime() / 1000;
			test.log(LogStatus.INFO, "Post API Response Code: " + responseObj.getStatusCode() + ", Time Elapsed: "
					+ elapsedTime + " seconds");
			String response = responseObj.getBody().asString();
			writeFile(test.getTest().getName() + "-" + dateFormat.format(new Date()) + ".txt",
					new StringBuffer(responseObj.asString()));
			JsonArray responseJsonArray = RestServiceUtils.getJSonResponse("[" + response + "]");
			JsonObject responseElements = responseJsonArray.get(0).getAsJsonObject();

			if (expectedOutputJson.contains("db.")) {
				JSONObject jsonObject = new JSONObject(responseElements.toString());
				Map<String, Object> map = JsonParser.loadJson(jsonObject);
				Map<String, Object> fmap = new HashMap<String, Object>();
				fmap.put("numRecords", "" + map.size());
				String titleName = null;
				for (int i = 0; i < map.size(); i++) {
					Map convertedMap = JsonParser.convertStringToMap(map.get("" + i).toString());
					titleName = titleName == null ? convertedMap.get(" doclist.docs.titleName").toString()
							: titleName + "," + convertedMap.get(" doclist.docs.titleName").toString();
				}
				fmap.put("titles", titleName);
				return validateResponseThroughDB(fmap, expectedOutputJson, null,"");

			}
			for (Map.Entry<String, JsonElement> responseEntry : responseElements.entrySet()) {
				String responseKey = responseEntry.getKey();
				if ("Response".equalsIgnoreCase(responseKey)) {
					String responseValue = responseEntry.getValue().toString();

					return validateResponse("[" + responseValue + "]", expectedOutputJson);
				} else {
					return validateResponse("[" + responseElements.getAsJsonObject().toString() + "]",
							expectedOutputJson);
				}
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Post request");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}
		test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Post request");
		return Constants.FAIL + ". Error occured";
	}

	public String validateResponse(String responseJson, String expectedJson) {
		JsonArray expectedJsonArray = RestServiceUtils.getJSonResponse("[" + expectedJson + "]");
		JsonArray responseJsonArray = RestServiceUtils.getJSonResponse(responseJson);
		JsonObject expectedElements = expectedJsonArray.get(0).getAsJsonObject();
		boolean expectedKeyFound = false;
		String expectedKey = "";
		for (Map.Entry<String, JsonElement> expectedEntry : expectedElements.entrySet()) {
			expectedKeyFound = false;
			expectedKey = expectedEntry.getKey();
			String expectedValue = expectedEntry.getValue().getAsString();
			if (expectedKey.contains("~")) {
				String key = expectedKey.replaceAll("~", "");
				if (verifyInArray(responseJsonArray, key, expectedValue)) {
					expectedKeyFound = true;
					test.log(LogStatus.PASS, key + " is found response");
				} else {
					return Constants.FAIL;
				}
			} else {
				JsonObject responseElements = responseJsonArray.get(0).getAsJsonObject();
				for (Map.Entry<String, JsonElement> responseEntry : responseElements.entrySet()) {
					String responseKey = responseEntry.getKey();
					if (expectedKey.equalsIgnoreCase(responseKey)) {
						expectedKeyFound = true;
						String responseValue = responseEntry.getValue().getAsString();
						if (expectedValue.equalsIgnoreCase(responseValue)) {
							test.log(LogStatus.PASS, expectedKey + " is found " + responseValue);
						} else {
							test.log(LogStatus.FAIL, expectedKey + " isn't found " + responseValue);
							return Constants.FAIL;
						}
					}
				}
			}
		}
		if (!expectedKeyFound) {
			test.log(LogStatus.FAIL, expectedKey + " is not found in response");
			return Constants.FAIL;
		}
		return Constants.PASS;
	}

	private boolean verifyInArray(JsonArray responseJsonArray, String key, String value) {
		boolean retVal = true;
		JsonObject responseElements = responseJsonArray.get(0).getAsJsonObject();
		for (Map.Entry<String, JsonElement> responseEntry : responseElements.entrySet()) {
			String responseKey = responseEntry.getKey();
			if (responseKey.equalsIgnoreCase("Docs") || responseKey.equalsIgnoreCase("groupTitleId_s")) {
				if (responseEntry.getValue().isJsonArray()) {
					JsonArray docArray = responseEntry.getValue().getAsJsonArray();
					for (int i = 0; i < docArray.size(); i++) {
						JsonElement doc = docArray.get(i);
						JsonObject docObj = doc.getAsJsonObject();
						if (null != docObj.get(key)) {
							if (!value.equalsIgnoreCase(docObj.get(key).getAsString())) {
								retVal = false;
							} else {
								test.log(LogStatus.PASS, key + ":" + value + " is verified in record [" + i + "]");
							}
						}
					}
				} else {
					if (null != responseEntry.getValue().getAsJsonObject().get(key)) {
						if (!value
								.equalsIgnoreCase(responseEntry.getValue().getAsJsonObject().get(key).getAsString())) {
							retVal = false;
						} else {
							test.log(LogStatus.PASS, key + ":" + value + " is verified in record");
						}
					}
				}
			}
		}
		return retVal;
	}

	public String getPersonAPI(String data) {
		test.log(LogStatus.INFO, "Search Data API");

		try {
			if (!RestServiceUtils.checkHttp_post("/Search", data)) {
				test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Search POST request for payload " + data);
				return Constants.FAIL + ". Error / Bad / Unknown Response";
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Search POST request for payload " + data);
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		test.log(LogStatus.PASS, "Pass. Response of Search POST request is OK for payload " + data);
		return Constants.PASS;
	}

	public String getPersonResponse(String data) {
		test.log(LogStatus.INFO, "Search functionality API response");

		try {
			response = RestServiceUtils.checkPost_Message("/Search", data);
			JsonArray jsonArray = RestServiceUtils.getJSonResponse(response);
			test.log(LogStatus.INFO, "Search API response--" + response + " for payload " + data);
			test.log(LogStatus.INFO, "First Search record--" + jsonArray.get(0));
		} catch (Exception e) {
			test.log(LogStatus.FAIL, ". Error / Bad / Unknown Response of Search POST request");
			return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
		}

		return Constants.PASS;
	}

	public String verifyGetPersonAPI(String data) {
		test.log(LogStatus.INFO, "Verify search result through API");

		try {

		} catch (Exception e) {

		}

		System.out.println("Actual----" + response);
		System.out.println("Expected----" + data);
		if (response.contains(data)) {
			System.out.println("PASSSSS");
			return Constants.PASS;

		} else {
			return Constants.FAIL;
		}
	}

	public String verifyBadData(String data) {
		test.log(LogStatus.INFO, "Verify Bad data through API");

		try {

		} catch (Exception e) {

		}

		int badcode = RestServiceUtils.getResponseHttp_post("/Post", data);

		if (badcode == 400) {
			return Constants.PASS;
		} else {
			return Constants.FAIL;
		}
	}

	public String verifyDuplicateData(String data) {
		test.log(LogStatus.INFO, "Verify Duplicate data through API");

		int duplicate;

		try {
			duplicate = RestServiceUtils.getResponseHttp_post("/Post", data);
		} catch (Exception e) {
			return Constants.FAIL;
		}

		if (duplicate == 300) {
			return Constants.PASS;
		} else {
			return Constants.FAIL;
		}
	}

	/******************************
	 * Reporting functions
	 ******************************/

	public void reportFailure(String failureMessage) {
		test.log(LogStatus.FAIL, failureMessage);
	}

	/**
	 * 
	 * @param cookie
	 * @param inputDataJson
	 * @param filterBy
	 * @param expectedOutputJson
	 * @return
	 */
	public String getFirstThenPostAPIDataUsingCookie(String cookie, String inputDataJson, String filterBy,String expectedOutputJson) {
		test.log(LogStatus.INFO, "API data ");
		String endPoint = "";
	
		JsonArray inputData = null;
		Response responseObj = null;
		Map<String, String> _response = null;
		String id = null;

		Map<String, String> _returnParamOutput = new HashMap<String, String>();
		Map<String, String> headers = new HashMap<String, String>();
		
		/**
		 * Store multiple call of API informations 
		 */
		List<String> inputDataList = new ArrayList<String>();  

		if (!inputDataJson.contains("~")) {
			inputDataList.add(inputDataJson);
		} else {
			inputDataList = Arrays.asList(inputDataJson.split("\\~")); 
		}
		
		/**
		 *  i -> This will iterate for multiple calls of API
		 */
		for (int i = 0; i < inputDataList.size(); i++) {
			
			if (i > 0 && _response != null && _returnParamOutput.size()==0)  // This will store list of title id's returned from first api call
			{ 
				if (inputDataList.get(i).toString().contains("?")|| inputDataList.get(i).toString().contains("GTMTitle")) {
					JsonArray jsonArray = RestServiceUtils.getJSonResponse("[" + _response.toString() + "]");
					JsonObject jsonObject = jsonArray.get(0).getAsJsonObject().get("docs").getAsJsonObject();

					for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
						String key = entry.getKey();
						JsonObject jsonInnerObject = jsonObject.get(key).getAsJsonObject();
						for (Map.Entry<String, JsonElement> nestedEntry : jsonInnerObject.entrySet()) {
							String nestedKey = nestedEntry.getKey();
							if (nestedKey.equalsIgnoreCase("sourceTitleId") && jsonInnerObject.get("nameSearchGroup")
									.toString().toLowerCase().contains(filterBy.toLowerCase())) {
								id = id == null ? nestedEntry.getValue().getAsString()
										: id + "," + nestedEntry.getValue().getAsString();
								_returnParamOutput.put("titleId_s", id); //This will store list of title id's returned from first api call
								break;
							}
						}
					}
				}
			}

			List<JsonArray> listInputArray = new ArrayList<JsonArray>();
			List<String> list = null;

			if (_returnParamOutput.size() > 0)  // This will execute only after first call
			{
				list = new ArrayList<String>(Arrays.asList(_returnParamOutput.get("titleId_s").split(",")));
				for (int cntr = 0; cntr < list.size(); cntr++) 
				{
					String rep = null;
					if (!inputDataList.get(i).toString().contains("#sgenno")) 
					{
						rep = "[" + inputDataList.get(i).toString().replace("titleId_s",
								"titleId_s=" + _returnParamOutput.get("titleId_s").split(",")[0]) + "]";

					} else 
					{
						rep = "[" + inputDataList.get(i).toString().replace("#sgenno", list.get(cntr).toString()) + "]";
					}
					inputData = RestServiceUtils.getJSonResponse(rep);
					listInputArray.add(inputData);
				}
			} else {
				inputData = RestServiceUtils.getJSonResponse("[" + inputDataList.get(i).toString() + "]"); // This will execute only for first call
				listInputArray.add(inputData);
			}

			
			//test.log(LogStatus.INFO, "Total " + listInputArray.size() + " record(s) are found ");

			for (int jCnt = 0; jCnt < listInputArray.size(); jCnt++)  // This will execute for stored title id which were returned after first API call
			{
				String bodyText = null;
				JsonObject elements = listInputArray.get(jCnt).get(0).getAsJsonObject();
				
				for (Map.Entry<String, JsonElement> entry : elements.entrySet()) {
					String key = entry.getKey();
					if (key.equalsIgnoreCase("EndPoint")) {
						endPoint = entry.getValue().getAsString();
					}
				}
				try {
					boolean getOrPost = false;
					if (i > 0 && !elements.toString().contains("?titleId_s=")) {
						bodyText = elements.get("Body").getAsString();
						responseObj = RestServiceUtils.http_postUsingCookies(headers, cookie, "application/json",
								endPoint, bodyText);
						getOrPost = true;
					} else {
						responseObj = RestServiceUtils.http_getUsingCookies(headers, cookie, "application/json",
								endPoint);
						getOrPost = false;
					}
					_response = JsonValidator.jsonParser(responseObj.asString());

					long elapsedTime = responseObj.getTime() / 1000;

					if (i > 0) { // i == inputDataList.size() - 1  // This call will occur only for second and onwards API calls
						Map<String, Object> map = null;
						if (getOrPost)
							map = (Map) JsonValidator.jsonParser(responseObj.asString()).get("Docs");
						else
							map = JsonValidator.jsonToMap(responseObj.getBody().asString());

						if (map.size() > 0) 
						{
							String queryParam= null;
							String comment=null;
							if(expectedOutputJson.contains(","))
							{  // Pattern of title ids would be  parent followed by childs
								if(i==1 && jCnt==0) {			// This will go for parent		
									queryParam=expectedOutputJson.split(",")[0];
									comment= "Season";
								}
								if(i==2 && jCnt>0)	{				// This will go for childs
									queryParam=expectedOutputJson.split(",")[1];
									comment="Episode";
								}
							}else
							{
								queryParam=expectedOutputJson; // This is only for first call
							}
							if(queryParam!=null) {
								test.log(LogStatus.INFO, "API Response Code: " + responseObj.getStatusCode() + ", Time Elapsed: "
										+ elapsedTime + " seconds");
								writeFile(test.getTest().getName() + dateFormat.format(new Date()) + ".txt",
										new StringBuffer(responseObj.asString()));

								validateResponseThroughDB(map, queryParam, list.get(jCnt),comment);
							}
						}
					}
				} catch (Exception e) {
					test.log(LogStatus.FAIL,
							". Error / Bad  Unknown Response of Get request or Exception" + e.getMessage());
					return Constants.FAIL + ". Exception " + e.getMessage() + " occured";
				}
			}
		}
		return Constants.PASS; // need to work on return type

	}

}
package com.birlasoft.gui.tests;

import java.awt.AWTException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.birlasoft.base.BaseTest;
import com.birlasoft.keywords.KeywordDriver;
import com.birlasoft.libs.Constants;
import com.birlasoft.libs.data.DataUtil;
import com.birlasoft.libs.data.Xls_Reader;
import com.relevantcodes.extentreports.LogStatus;

public class UpdateRecordTest extends BaseTest {
	@BeforeTest
	public void init() {
		xls = new Xls_Reader(Constants.TMobileGUI_XLS);
		testName = "UpdateRecordTest";
	}

	@Test(dataProvider = "getData")
	public void updateRecordTest(Hashtable<String, String> data) throws InterruptedException, AWTException {
		test = rep.startTest(testName);
		test.log(LogStatus.INFO, data.toString());

		if (DataUtil.isSkip(xls, testName) || data.get("Runmode").equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as runmode is N");
			throw new SkipException("Skipping the test as runmode is N");
		}

		test.log(LogStatus.INFO, "Starting " + testName);

		app = new KeywordDriver(test);
		test.log(LogStatus.INFO, "Executing keywords");
		app.executeKeywords(testName, xls, data);
		
		test.log(LogStatus.PASS, "PASS");
		app.getGenericKeyWords().takeScreenShot();
		app.getGenericKeyWords().closeBrowser();
	}
}
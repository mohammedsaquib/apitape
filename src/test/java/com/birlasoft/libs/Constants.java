package com.birlasoft.libs;

import org.openqa.selenium.By;

public class Constants {
	public static final String NetAPI_XLS = System.getProperty("user.dir") + "//data//API.xlsx";
	public static final String TMobileGUI_XLS = System.getProperty("user.dir") + "//data//GUI.xlsx";

	public static final String Project_FILE_PATH = System.getProperty("user.dir")
			+ "//src//test//resources//project.properties";

	public static final String KEYWORDS_SHEET = "Keywords";

	public static final String TCID_COL = "TCID";
	
	public static final String KEYWORD_COL = "Keyword";
	public static final String TESTTYPE_COL = "TestType";
	public static final String OBJECT_COL = "Object";
	public static final String DATA_COL = "Data";
	public static final String TESTCASES_SHEET = "TestCases";
	public static final String RUNMODE_COL = "Runmode";
	public static final String REPORT_PATH = "Test_Results\\report\\";
	public static final String SCREENSHOT_PATH = "Test_Results\\report\\screenshots\\";
	public static final String ARCHIVE_PATH = "Archive\\";
	public static final String PASS = "PASS";
	public static final String FAIL = "FAIL";
	
	public static final String CONDITION_SEPARATOR = ";";
	public static final String CONDITIONVALUE_SEPARATOR = "=";
	public static final String COLUMN_SEPARATOR = "=";
	public static final String COLUMNVALUE_SEPARATOR = "::::";
	// Change according to spinner in application
	public static final By BYSPINNER = By.className("loader");
	
	//Database Query Keywords
	public static final String SEARCH_QUERY_BY_Title_Name="db.titleQuery";
	public static final String SEARCH_QUERY_BY_Series_Name1="db.searchBySeriesNameQuery1";	
	public static final String SEARCH_QUERY_BY_Series_Name2="db.searchBySeriesNameQuery2";
}
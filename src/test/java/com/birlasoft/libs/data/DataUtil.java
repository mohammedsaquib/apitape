package com.birlasoft.libs.data;


import java.util.ArrayList;
import java.util.Hashtable;
import com.birlasoft.libs.Constants;



public class DataUtil {
	public static Object[][] getData(Xls_Reader xls) {
		String sheetName = "Data";
		int iCount = 2;
		int index = 0;

		Hashtable<String, Hashtable> l = null;
		ArrayList<Object> dataList=new ArrayList<Object>();
		
		int rowCount = xls.getRowCount(Constants.TESTCASES_SHEET);
		int rowYesCount = xls.getYesRowCount(Constants.TESTCASES_SHEET);
		
		Object[][] data = new Object[rowYesCount][1];
		
		while (iCount <= rowCount) {
			 dataList=new ArrayList<Object>();
			String testName = xls.getCellData(Constants.TESTCASES_SHEET, 0, iCount);
			boolean skip = DataUtil.isSkip(xls, testName);

			if (!skip) {

				// reads data for only testCaseName
				int testStartRowNum = 1;

				while (!xls.getCellData(sheetName, 0, testStartRowNum).equals(testName)) {
					testStartRowNum++;
				}
				System.out.println("");
				System.out.println("testcase name - " + testName);
				System.out.println("Test starts from row - " + testStartRowNum);

				int colStartRowNum = testStartRowNum + 1;
				int dataStartRowNum = testStartRowNum + 2;

				// calculate rows of data
				int rows = 0;

				while (!xls.getCellData(sheetName, 0, dataStartRowNum + rows).equals("")) {
					rows++;
				}

				System.out.println("Total rows are  - " + rows);

				// calculate total cols
				int cols = 0;

				while (!xls.getCellData(sheetName, cols, colStartRowNum).equals("")) {
					cols++;
				}

				System.out.println("Total cols are --  " + cols);
				// data = new Object[rows][1];

				// read the data
				int dataRow = 0;
				Hashtable<String, String> table = null;

				Object[][] itrData = new Object[rows][1];
				ArrayList list = new ArrayList(); 
				for (int rNum = dataStartRowNum; rNum < dataStartRowNum + rows; rNum++) {
					table = new Hashtable<String, String>();
					l = new Hashtable<String, Hashtable>();
					for (int cNum = 0; cNum < cols; cNum++) {
						String key = xls.getCellData(sheetName, cNum, colStartRowNum);
						String value = xls.getCellData(sheetName, cNum, rNum);
						table.put(key, value);
					}

					l.put(testName, table);
					
					//itrData[dataRow][0] = l;
					list.add(l);
					dataRow++;
				}
				
				dataList.add(list);
				//dataList.add(itrData);
				data[index][0] = dataList;
				index++;
			}
			
			iCount++;
		}
		
		
		return data;

	}

	// true - N
	// false - Y
	public static boolean isSkip(Xls_Reader xls, String testName) {
		int rows = xls.getRowCount(Constants.TESTCASES_SHEET);

		for (int rNum = 2; rNum <= rows; rNum++) {
			String tcid = xls.getCellData(Constants.TESTCASES_SHEET, Constants.TCID_COL, rNum);
			if (tcid.equals(testName)) {
				String runmode = xls.getCellData(Constants.TESTCASES_SHEET, Constants.RUNMODE_COL, rNum);
				if (runmode.equals("Y"))
					return false;
				else
					return true;
			}
		}

		return true;
	}
}
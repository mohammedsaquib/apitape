package com.birlasoft.libs.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class RestServiceUtils {
	public static String baseURI;

	public static void setBaseURI(String baseURI) {
		RestServiceUtils.baseURI = baseURI;
		RestAssured.baseURI = baseURI;
	}

	public static boolean checkSuccessResponse(String endPoint) {
		HttpURLConnection connection;
		URL url = null;
		boolean result = false;

		try {
			url = new URL(baseURI + endPoint);
		} catch (MalformedURLException e1) {
		}

		try {
			connection = (HttpURLConnection) url.openConnection();

			connection.connect();

			result = connection.getResponseCode() == HttpURLConnection.HTTP_OK;
			connection.disconnect();
		} catch (IOException e) {
		}

		return result;
	}

	public static String http_get(String endPoint) {
		HttpURLConnection connection;
		URL url = null;
		StringBuilder output = new StringBuilder();

		try {
			url = new URL(baseURI + endPoint);
		} catch (MalformedURLException e1) {
		}

		try {
			connection = (HttpURLConnection) url.openConnection();

			connection.connect();

			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String input;

			while ((input = br.readLine()) != null) {
				output.append(input);
			}

			br.close();

			connection.disconnect();
		} catch (IOException e) {
		}

		return output.toString();
	}

	public static JsonArray getJSonResponse(String response) {
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArray = (JsonArray) jsonParser.parse(response);
		return jsonArray;
	}
	
	public static int getResponseHttp_post(String endPoint, String requestPayload) {
		HttpURLConnection connection;
		URL url = null;
		int responseCode = 0;
		
		try {
			url = new URL(baseURI + endPoint);
		} catch (MalformedURLException e1) {
		}

		try {
			connection = (HttpURLConnection) url.openConnection();
			
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(requestPayload);
			wr.flush();
			wr.close();

			responseCode = connection.getResponseCode();
			
			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return responseCode;
	}
	
	public static boolean checkHttp_post(String endPoint, String requestPayload) {
		HttpURLConnection connection;
		URL url = null;
		boolean result = false;
		
		try {
			url = new URL(baseURI + endPoint);
		} catch (MalformedURLException e1) {
		}

		try {
			connection = (HttpURLConnection) url.openConnection();
			
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(requestPayload);
			wr.flush();
			wr.close();

			result = connection.getResponseCode() == HttpURLConnection.HTTP_OK;

			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static String checkPost_Message(String endPoint, String requestPayload){
		HttpURLConnection connection;
		URL url = null;
		StringBuffer response = new StringBuffer();
		
		try {
			url = new URL(baseURI + endPoint);
		} catch (MalformedURLException e1) {
		}

		try {	
			connection = (HttpURLConnection) url.openConnection();
			
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setRequestProperty("Content-Type","application/json;charset=UTF-8");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(requestPayload);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			
			in.close();

			connection.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 return response.toString();
	}
	
	public static Response http_get(Map<String, String> headers, String username, String password, String contentType, String url) 
	{
		Response resp = RestAssured.given()
				.auth().basic(username, password)
				.headers(headers)
				.contentType(contentType).get(url);
		return resp;
	}
	
	public static Response http_getUsingCookies(Map<String, String> headers, String cookie, String contentType, String url) 
	{
		Response resp = RestAssured.given()
				.cookie(cookie)
				.headers(headers)
				.contentType(contentType).get(url);
		System.out.println("request completed for get:->"+resp.asString());
		return resp;
	}
	
	public static Response http_postUsingCookies(Map<String, String> headers, String cookie, String contentType, String url, String body) 
	{
		Response resp = RestAssured.given()
				.cookie(cookie)
				.headers(headers)
				.contentType(contentType)	
				.body(body)
				.post(url)
				.andReturn();
		System.out.println("request completed for post:->"+resp.asString());
		return resp;
	}
}
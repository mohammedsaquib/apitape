package com.birlasoft.libs.ui;

public enum BrowserTypes {
	CHROME, IE, FIREFOX, SAFARI, HTMLUNIT
}
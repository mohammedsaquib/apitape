package com.birlasoft.base;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.util.Date;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.birlasoft.keywords.AppKeywords;
import com.birlasoft.keywords.KeywordDriver;
import com.birlasoft.libs.Constants;
import com.birlasoft.libs.ExtentManager;
import com.birlasoft.libs.FileHandlingUtils;
import com.birlasoft.libs.data.DataUtil;
import com.birlasoft.libs.data.Xls_Reader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class BaseTest {
	public ExtentReports rep = ExtentManager.getInstance();
	public ExtentTest test;
	public KeywordDriver app;
	public Xls_Reader xls;
	public String testName;
	AppKeywords appKeywords;

	@BeforeSuite
	public void archiveResults() throws IOException {
		String todaysDate = DateFormat.getDateTimeInstance().format(new Date()).toString().replaceAll(":", "_")
				.replaceAll("\\s+", "_").replaceAll(",", "");
		String userName = System.getProperty("user.name");
		String hostName = "Global";

		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}

		String reportZip = Constants.ARCHIVE_PATH + File.separatorChar + hostName + "_" + userName + "_Report_"
				+ todaysDate + ".zip";

		FileHandlingUtils.createDirectory(new File(Constants.ARCHIVE_PATH).getAbsolutePath());
		FileHandlingUtils.zipDir(new File(Constants.REPORT_PATH).getAbsolutePath(), new File(reportZip).getAbsolutePath());
		FileHandlingUtils.deleteDirectory(new File(Constants.REPORT_PATH).getAbsolutePath());
		FileHandlingUtils.createDirectory(new File(Constants.REPORT_PATH).getAbsolutePath());
		FileHandlingUtils.createDirectory(new File(Constants.REPORT_PATH).getAbsolutePath());
		testName = "LoginApplication";
		test = rep.startTest(testName);
		System.out.println("Logging into the GTM application");
		app = new KeywordDriver(test);
		appKeywords = new AppKeywords(test);
		String result = appKeywords.openBrowser(appKeywords.prop.getProperty("browserType"));
		result = appKeywords.navigate("gtmURL");
		result = appKeywords.input("gtm_username", appKeywords.prop.getProperty("gtmUser"));
		result = appKeywords.input("gtm_password", appKeywords.prop.getProperty("gtmPassword"));
		result = appKeywords.click("gtm_submit");
		System.out.println("Successfully logged into the GTM application");
	}
	
	
	@AfterSuite
	public void closeBrowser() {
		appKeywords.closeBrowser();
		if (rep != null) 
			rep.endTest(test);
			rep.flush();
	}
	
	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls);
	}
}
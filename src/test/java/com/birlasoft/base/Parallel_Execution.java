package com.birlasoft.base;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.testng.TestNG;
import org.testng.collections.Lists;

public class Parallel_Execution {
	public static void main(String[] args)
			throws FileNotFoundException, ParserConfigurationException, IOException, org.xml.sax.SAXException {
		TestNG testng = new TestNG();
		
		List<String> suites = Lists.newArrayList();
		suites.add("src\\test\\resources\\GUI.xml");
		suites.add("src\\test\\resources\\API.xml");
		testng.setTestSuites(suites);

		testng.setSuiteThreadPoolSize(3);
		testng.run();
	}
}
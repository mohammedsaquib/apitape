package com.birlasoft.validator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.minidev.json.parser.ParseException;


/**
 * 
 * 
 * @author amit.nigam
 * @description: This class is under development and freezed only for few fixed scenarios
 */
public class JsonParser {
	static HashMap<String, Object> myKeyValues = new HashMap<String, Object>();
	static Stack<String> ckeys_path = new Stack<String>();
	static HashMap<String, Object> mycKeyValues = new HashMap<String, Object>();
	static boolean flag = false;
	static Stack<String> key_path = new Stack<String>();

	public static HashMap loadJson(JSONObject json) {

		Iterator<?> json_keys = json.keys();
		System.out.println(" ");
		while (json_keys.hasNext()) {
			String json_key = (String) json_keys.next();
			String json_val = null;
			try {
				key_path.push(json_key);

				loadJson(json.getJSONObject(json_key));
			} catch (JSONException e) {
				String key = "";
				for (String sub_key : key_path) {
					key += sub_key + ".";
				}

				key = key.substring(0, key.length() - 1);
				try {
					json_val = "" + json.get(json_key);
					if (json_val.startsWith("[{")) {
						flag = true;
						int size = new JSONArray(json_val).toList().size();
						for (int i = 0; i < size; i++) {
							myKeyValues.put("" + i,
									loadSubJson(i, new JSONArray(json_val).getJSONObject(i)).toString());
						}
					}
				} catch (Exception ex) {
					json_val = "none";
				}
				key_path.pop();
				
				myKeyValues.put(key, json_val);
			}
		}
		if (key_path.size() > 0) {
			key_path.pop();
		}
		
			HashMap<String,Object> finalMap= new HashMap<String,Object>();
			if(myKeyValues.containsKey("0"))
			{
				for(Entry<String, Object> entry:myKeyValues.entrySet()) {
					try {
						if((Object)Integer.parseInt(entry.getKey()) instanceof Integer){
							finalMap.put(entry.getKey(), entry.getValue());
						}
					}catch(Exception ex) {
						
						
					}
				}
			}
		
		return finalMap.size()>0?finalMap:myKeyValues;
	}

	public static HashMap<String,String> convertStringToMap(String response) {
		response = response.replace("{", "").replace("}", "").trim();
		String[] pairs=response.split(",");
		HashMap<String,String> myMap = new HashMap<String,String>();
		for (int i=0;i<pairs.length;i++) {
		    String pair = pairs[i];
		    try {
		    String[] keyValue = pair.split("=");
		//    System.out.println("" +keyValue[0] + ":->"+keyValue[1].replace("[", "").replace("]", ""));
		    myMap.put(keyValue[0], keyValue[1]);
		    }catch(Exception ex) {}
		}
		return myMap;
	}
	
	
	public static Map<String, Object> asMap(Object... args) {
	    Map<String, Object> argMap = new HashMap<String, Object>();
	    for (int i = 0; i < args.length; i += 2) {
	      String key;
	      try {
	        key = (String) args[i];
	      } catch (ClassCastException cce) {
	        System.err.println(cce.getMessage());
	        System.err.println("args[" + i + "] " + args[i].toString());
	        throw cce;
	      }
	      if (i + 1 < args.length) {
	        Object value = args[i + 1];
	        argMap.put(key, value);
	      }
	    }
	    return argMap;
	  }
	private static HashMap loadSubJson(int number, JSONObject json) {

		Iterator<?> json_keys = json.keys();

		while (json_keys.hasNext()) {
			String json_key = (String) json_keys.next();
			String json_val = null;
			try {
				ckeys_path.push(json_key);

				loadSubJson(number, json.getJSONObject(json_key));
			} catch (JSONException e) {
				String key = "";
				for (String sub_key : ckeys_path) {
					key += sub_key + ".";
				}
				key = key.substring(0, key.length() - 1);
				try {
					json_val = "" + json.get(json_key);
					if (json_val.startsWith("[{")) {
						int size = new JSONArray(json_val).toList().size();

						for (int i = 0; i < size; i++) {
							loadSubJson(i + number, new JSONArray(json_val).getJSONObject(i));
						}
					}
				} catch (Exception ex) {
					json_val = "none";
				}
				System.out.println(key + ": " + json_val);

				ckeys_path.pop();
				if (!key.equalsIgnoreCase("groupTitleId_s.matches"))
					mycKeyValues.put(key, json_val);

			}
		}
		if (ckeys_path.size() > 0) {
			ckeys_path.pop();
		}
		return mycKeyValues;
	}

	public static void main(String[] args) throws ParseException {

		try {
			// read the json file
			String x = "{\"groupTitleId_s\":{\"matches\":4,\"ngroups\":4,\"groups\":[{\"groupValue\":\"307494\",\"doclist\":{\"numFound\":1,\"start\":0,\"docs\":[{\"shortName\":\"FLASH GORDON\",\"sortName\":\"FLASH GORDON ('36)\",\"seriesTitleName\":\"\",\"titleNameDelimited\":\" FLASH GORDON ('36) \",\"titleNameDelimitedFuzzy\":\" FLASH GORDON ('36) \",\"insertUser\":\"Elizabeth Chase [500993787]\",\"titleNameFuzzy\":\"FLASH GORDON ('36)\",\"groupTitleName\":\" FLASH GORDON ('36) \",\"grandParentTitleName\":\"\",\"genre\":[\"Science Fiction\"],\"parentTitleName\":\"\",\"levelTagDesc\":\"Title (Non-Hierarchical)\",\"updateUser\":\"Braxton Perkins [206048347]\",\"pId\":\"155d75a1-6823-4c96-9c43-3b6e9ddb2d8b\",\"timestamp\":\"2017-08-30T01:19:39\",\"year\":1936,\"titleId\":307494,\"releaseStatusDesc\":\"Released\",\"insertDate\":\"2003-12-12T19:59:52\",\"performanceType\":\"Live Action\",\"primaryFinancialOwner\":\"V130 - Universal Studios Home Video\",\"primaryLegalOwner\":\"V130 - Universal Studios Home Video\",\"statusPriority\":1,\"originalNetwork\":\"USA\",\"pilotFlag\":\"No\",\"originalMarket\":\"Theatrical\",\"titleName\":\"FLASH GORDON ('36)\",\"titleLevel\":1,\"releaseAirYear\":1936,\"allowLiquidation\":\"No\",\"dataSource\":\"GTM\",\"duplicateRequestFlag\":\"N\",\"deactivationDate\":\"2003-12-15T08:00:00\",\"updateDate\":\"2003-12-16T00:32:32\",\"recMedia\":\"Film\",\"groupTitleId\":307494,\"primaryRoyaltyOwner\":\"4000 - PARIS - Domestic\",\"primaryOriginCode\":\"038 - External Network\",\"productType\":\"Feature\",\"activeFlag\":\"No\",\"digitalFlag\":\"No\",\"libraryFlag\":\"No\",\"hierarchicalFlag\":\"N\",\"docType\":\"Title\",\"phantomFlag\":\"N\",\"groupTitleId_s\":\"307494\",\"titleId_s\":\"307494\",\"imageAvailable\":\"N\",\"eraProductId\":307494,\"_version_\":1577117002567254016,\"payTVreportingGroup\":\"Pay TV\"}]}},{\"groupValue\":\"30522\",\"doclist\":{\"numFound\":1,\"start\":0,\"docs\":[{\"shortName\":\"APOLLO 13\",\"sortName\":\"APOLLO 13\",\"eidrT\":\"10.5240/CF46-E8CD-0F91-6771-B1A4-N\",\"groupImageLocation\":\".\\\\Images\\\\Thumbnails\\\\TM_30522.jpg\",\"seriesTitleName\":\"\",\"titleNameDelimited\":\" APOLLO 13 \",\"titleNameDelimitedFuzzy\":\" APOLLO 13 \",\"casts\":[\" Kevin Bacon (I) \",\" Tom Hanks \",\" Ed Harris (I) \",\" Bill Paxton \",\" Kathleen Quinlan \",\" Gary Sinise \"],\"titleNameFuzzy\":\"APOLLO 13\",\"eidrV\":[\"10.5240/3359-A108-B675-4930-B99F-O\"],\"directors\":[\" Ron Howard (I) \"],\"talentnametitle\":[\" Kevin Bacon (I) \",\" William Broyles \",\" Dean Cundey \",\" Brian Grazer \",\" Todd Hallowell \",\" Tom Hanks \",\" Ed Harris (I) \",\" Ron Howard (I) \",\" Jeffrey Kluger \",\" James Lovell \",\" Bill Paxton \",\" Kathleen Quinlan \",\" Al Reinhert \",\" Gary Sinise \"],\"imageLocation\":\".\\\\Images\\\\Thumbnails\\\\TM_30522.jpg\",\"groupTitleName\":\" APOLLO 13 \",\"grandParentTitleName\":\"\",\"genre\":[\"Adventure\",\"Drama\",\"Historical\"],\"parentTitleName\":\"\",\"levelTagDesc\":\"Title (Non-Hierarchical)\",\"synopsis\":[\" Nominated for nine Academy Awards®, Apollo 13 is the tremendously gripping true story of the 1970's ill-fated moon voyage. When a fuel leak threatens to strand three astronauts in space, the ground crew and the mission team must struggle to avert catastrophe. Their solutions are ingenious, the acting - led by Oscar®-winner Tom Hanks - is top notch, and the special effects are seamless. Directed by Academy Award®-winner Ron Howard. \"],\"updateUser\":\"Saravanan Sourirajan [502188393]\",\"akanametitle\":[\" LOST MOON: THE PERILOUS FLIGHT OF APOLLO 13 \"],\"pId\":\"ba67d00b-a387-40c3-bffc-0a0b4f904c59\",\"timestamp\":\"2017-08-30T01:19:10\",\"year\":1995,\"titleId\":30522,\"releaseStatusDesc\":\"Released\",\"performanceType\":\"Live Action\",\"primaryFinancialOwner\":\"U424 - Universal Pictures, A Division Of Ucsp\",\"primaryLegalOwner\":\"U424 - Universal Pictures, A Division Of Ucsp\",\"statusPriority\":1,\"locationCountry\":\"United States\",\"titleUpdateDate\":\"2016-11-02T00:50:36\",\"actualOriginalAirdate\":\"1995-06-30T07:00:00\",\"originalNetwork\":\"USA\",\"pilotFlag\":\"No\",\"originalMarket\":\"Theatrical\",\"titleName\":\"APOLLO 13\",\"titleLevel\":1,\"releaseAirYear\":1995,\"allowLiquidation\":\"No\",\"dataSource\":\"GTM\",\"duplicateRequestFlag\":\"N\",\"goldenRecordFlag\":\"Y\",\"updateDate\":\"2017-01-04T03:27:03\",\"recMedia\":\"Film\",\"slateYear\":1995,\"eventDateTitle\":\"1995-06-28T07:00:00\",\"groupTitleId\":30522,\"primaryProductionEntityOwner\":\"U470 - Universal Pictures Productions\",\"requestId\":578430,\"timeSlotCode\":\"120\",\"primaryRoyaltyOwner\":\"4000 - PARIS - Domestic\",\"productionNo\":\"06986\",\"primaryOriginCode\":\"010 - Universal Pictures\",\"productType\":\"Feature\",\"activeFlag\":\"Yes\",\"digitalFlag\":\"No\",\"answerPrintStatus\":\"Answer Print\",\"libraryFlag\":\"Yes\",\"hierarchicalFlag\":\"N\",\"docType\":\"Title\",\"phantomFlag\":\"N\",\"groupTitleId_s\":\"30522\",\"titleId_s\":\"30522\",\"imageAvailable\":\"Y\",\"eraProductId\":2482,\"_version_\":1577116972013846529,\"payTVreportingGroup\":\"Theatrical\"}]}},{\"groupValue\":\"44266\",\"doclist\":{\"numFound\":1,\"start\":0,\"docs\":[{\"shortName\":\"DANTE'S PEAK\",\"sortName\":\"DANTE'S PEAK\",\"eidrT\":\"10.5240/FC56-8909-2EE2-47C5-BEA2-V\",\"groupImageLocation\":\".\\\\Images\\\\Thumbnails\\\\TM_44266.jpg\",\"seriesTitleName\":\"\",\"titleNameDelimited\":\" DANTE'S PEAK \",\"titleNameDelimitedFuzzy\":\" DANTE'S PEAK \",\"casts\":[\" Pierce Brosnan \",\" Charles Hallahan \",\" Linda Hamilton \",\" Grant Heslov \",\" Elizabeth Hoffman \",\" Jamie Smith \"],\"titleNameFuzzy\":\"DANTE'S PEAK\",\"eidrV\":[\"10.5240/A71C-BE54-9E8C-796A-3AEA-F\"],\"directors\":[\" Roger Donaldson \"],\"talentnametitle\":[\" Andrzej Bartkowiak \",\" Leslie Bohem \",\" Pierce Brosnan \",\" Roger Donaldson \",\" Charles Hallahan \",\" Linda Hamilton \",\" Ilona Herzberg \",\" Grant Heslov \",\" Elizabeth Hoffman \",\" Gale Hurd \",\" G Singer \",\" Joseph Singer \",\" Jamie Smith \"],\"imageLocation\":\".\\\\Images\\\\Thumbnails\\\\TM_44266.jpg\",\"groupTitleName\":\" DANTE'S PEAK \",\"grandParentTitleName\":\"\",\"genre\":[\"Thriller\"],\"parentTitleName\":\"\",\"levelTagDesc\":\"Title (Non-Hierarchical)\",\"synopsis\":[\" A volcanologist travels to an idyllic Northwest town when a long-dormant volcano shows signs of a violent reawakening \"],\"updateUser\":\"Paridhi Purohit [206452960]\",\"akanametitle\":[\" DANTE POKLA \",\" DANTE YANARDAGI \",\" DANTEJEV VRH \",\" DANTEOV VRH \",\" DANTES PEAK \",\" DANTE'S PEAK \",\" DANTE'S PEAK (FINNISH AND SWEDISH) \",\" DANTE'S PEAK: LA FURIA DE LA MONTANA \",\" DANTE'S PEAK: LA FURIA DELLA MONTAGNA \",\" DANTE'S PEAK: LE SOMMET DE DANTE \",\" DANTE'S PEAK: ROZPOUTANE PEKLO (CZECH) R \",\" DANTE'S PEAK: THORRANEE FAI NAROK TALOM \",\" DANTE'S PEAK: TIAN BENG DI LIE (LOCAL LA \",\" GADAB EL GABAL: PEAK \",\" GORA DANTEGO \",\" I KORYFI TOU DANTE \",\" LE PIC DE DANTE: RIEN N'ARRETE LA COLERE \",\" LOCAL LANGUAGE (URDU SCRIPT) \",\" O CUME DE DANTE \",\" O INFERNO DE DANTE \",\" PISGAT DANTE \",\" TINDUR DANTES \",\" UN PUEBLO LLAMADO DANTE'S PEAK \",\" VARHAT NA DANTE (LOCAL LANGUAGE) \",\" VIRFUL LUI DANTE: DANTE'S PEAK \"],\"pId\":\"c8fb375d-cb34-48f1-b9df-786ef9ab745a\",\"timestamp\":\"2017-08-30T01:19:11\",\"year\":1997,\"titleId\":44266,\"releaseStatusDesc\":\"Released\",\"performanceType\":\"Live Action\",\"primaryFinancialOwner\":\"U424 - Universal Pictures, A Division Of Ucsp\",\"primaryLegalOwner\":\"U424 - Universal Pictures, A Division Of Ucsp\",\"statusPriority\":1,\"locationCountry\":\"United States\",\"titleUpdateDate\":\"2014-11-05T19:17:26\",\"actualOriginalAirdate\":\"1997-02-07T08:00:00\",\"locationState\":\"Idaho\",\"originalNetwork\":\"USA\",\"pilotFlag\":\"No\",\"originalMarket\":\"Theatrical\",\"titleName\":\"DANTE'S PEAK\",\"titleLevel\":1,\"releaseAirYear\":1997,\"allowLiquidation\":\"No\",\"dataSource\":\"GTM\",\"duplicateRequestFlag\":\"N\",\"goldenRecordFlag\":\"Y\",\"updateDate\":\"2017-04-17T01:37:53\",\"recMedia\":\"Film\",\"slateYear\":1997,\"eventDateTitle\":\"1997-02-05T08:00:00\",\"groupTitleId\":44266,\"primaryProductionEntityOwner\":\"U470 - Universal Pictures Productions\",\"requestId\":427429,\"timeSlotCode\":\"120\",\"primaryRoyaltyOwner\":\"4000 - PARIS - Domestic\",\"productionNo\":\"02244\",\"primaryOriginCode\":\"010 - Universal Pictures\",\"productType\":\"Feature\",\"activeFlag\":\"Yes\",\"digitalFlag\":\"No\",\"answerPrintStatus\":\"Answer Print\",\"libraryFlag\":\"Yes\",\"hierarchicalFlag\":\"N\",\"docType\":\"Title\",\"phantomFlag\":\"N\",\"groupTitleId_s\":\"44266\",\"titleId_s\":\"44266\",\"imageAvailable\":\"Y\",\"eraProductId\":2495,\"_version_\":1577116973391675392,\"payTVreportingGroup\":\"Theatrical\"}]}},{\"groupValue\":\"212446\",\"doclist\":{\"numFound\":1,\"start\":0,\"docs\":[{\"vistaCompanyNumber\":\"627\",\"sortName\":\"MEHGAN'S DEEP DIVER JOURNALS\",\"seriesTitleName\":\"\",\"titleNameDelimited\":\" MEHGAN'S DEEP DIVER JOURNALS \",\"titleNameDelimitedFuzzy\":\" MEHGAN'S DEEP DIVER JOURNALS \",\"titleNameFuzzy\":\"MEHGAN'S DEEP DIVER JOURNALS\",\"talentnametitle\":[\" Chris Sloan \"],\"groupTitleName\":\" MEHGAN'S DEEP DIVER JOURNALS \",\"grandParentTitleName\":\"\",\"genre\":[\"*Unknown\"],\"parentTitleName\":\"\",\"levelTagDesc\":\"Title (Non-Hierarchical)\",\"updateUser\":\"Elizabeth Chase [500993787]\",\"akanametitle\":[\" SWIMMING/SHARKS-MEHGAN'S DEEPDIVER JRNLS \"],\"pId\":\"b1797a1a-009d-4047-8b86-14af39a9b7b1\",\"timestamp\":\"2017-08-30T01:19:31\",\"year\":1901,\"titleId\":212446,\"releaseStatusDesc\":\"Released\",\"insertDate\":\"2001-12-10T08:00:00\",\"primaryFinancialOwner\":\"F073 - USA Cable Entertainment LLC\",\"primaryLegalOwner\":\"F073 - USA Cable Entertainment LLC\",\"statusPriority\":1,\"originalNetwork\":\"USA\",\"pilotFlag\":\"No\",\"originalMarket\":\"Theatrical\",\"titleName\":\"MEHGAN'S DEEP DIVER JOURNALS\",\"titleLevel\":1,\"releaseAirYear\":1901,\"allowLiquidation\":\"No\",\"dataSource\":\"GTM\",\"duplicateRequestFlag\":\"N\",\"deactivationDate\":\"2004-03-17T08:00:00\",\"updateDate\":\"2004-03-17T19:49:39\",\"groupTitleId\":212446,\"primaryProductionEntityOwner\":\"F073 - USA Cable Entertainment LLC\",\"primaryRoyaltyOwner\":\"4000 - PARIS - Domestic\",\"primaryOriginCode\":\"041 - First Run\",\"productType\":\"Feature\",\"activeFlag\":\"No\",\"digitalFlag\":\"No\",\"libraryFlag\":\"No\",\"hierarchicalFlag\":\"N\",\"docType\":\"Title\",\"phantomFlag\":\"N\",\"groupTitleId_s\":\"212446\",\"titleId_s\":\"212446\",\"imageAvailable\":\"N\",\"eraProductId\":212446,\"_version_\":1577116994799403010,\"payTVreportingGroup\":\"Theatrical\"}]}}]},\"matches\":null,\"ngroups\":null}";
			// String x="{\"shortName\":\"MUCH ADO ABOUT\",\"sortName\":\"MUCH ADO ABOUT NANA\",\"imageLocation\":\".\\\\Images\\\\Thumbnails\\\\TM_664391.png\",\"genre\":[\"Comedy\",\"Family\"],\"levelTagDesc\":\"Episode\",\"synopsis\":[\"Jill's computer crashes, so she has to go into work and recreate her researchpaper, forcing Tim to take Mark to Swan Lake instead of her. Tim reluctantlyagrees, but when Wilson gives him front-row courtside seats to the Pistonsgame, he decides to leave the ballet after thirty minutes and go to the gameinstead. Brad and Randy are left home alone, time they spend smoking one ofTim's cigars from his stash.\"],\"releaseStatusDesc\":\"Released\",\"originalNetwork\":\"ABC\",\"productType\":\"Series\",\"pilotFlag\":\"No\",\"activeFlag\":\"Yes\",\"digitalFlag\":\"No\",\"originalMarket\":\"U.S.Network - PrimeTime\",\"libraryFlag\":\"No\",\"hierarchicalFlag\":\"Y\",\"titleName\":\"MUCHADO ABOUT NANA\",\"allowLiquidation\":\"No\"}";
			JSONObject o = new JSONObject(x);
			JsonParser t = new JsonParser();

			System.out.println(t.loadJson(o));

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
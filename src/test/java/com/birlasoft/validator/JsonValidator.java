package com.birlasoft.validator;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import com.birlasoft.libs.Constants;
import com.birlasoft.libs.data.DBUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;

public class JsonValidator {
	private static Properties prop = new Properties();
	static {
		try {
			FileInputStream fs = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//project.properties");
			prop.load(fs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Map<Integer, HashMap> getDBDetails(String queryKey, ExtentTest test,String... parameters) {
		Map<Integer, HashMap> resultMap = new HashMap<Integer, HashMap>();
		DBUtils dbUtils = DBUtils.getInstance();
		try {
			resultMap = dbUtils.executeQuery(test,prop.getProperty("db.driver"), prop.getProperty("db.connection"), prop.getProperty(queryKey), true, parameters);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	public String validateResponse(String queryKey, String parameters, String responseJson) {
		getDBDetails(queryKey,null, parameters);
		return Constants.PASS;
	}
	
	public static void main(String args[]) {
		JsonValidator jsonValidator = new JsonValidator();
		jsonValidator.jsonParser("{\"numFound\":1351553,\"start\":0,\"docs\":[{\"genreDesc\":\"Comedy [COM]\",\"insertUser\":\"Devin Tyus [206414020]\",\"pId\":\"6400a3a4-88fa-4967-8fcf-3c8f14a9fc68\",\"timestamp\":\"2017-08-11T22:08:33\",\"titleId\":596970,\"insertDate\":\"2015-10-09T08:52:39\",\"primaryGenre\":\"Y\",\"primaryGenreFlag\":\"Y\",\"dataSource\":\"GTM\",\"docType\":\"Genre\",\"titleId_s\":\"596970\",\"_version_\":1575474234527842310}]}");
	}
	
	public static Map jsonParser(String response) {
		Map<String, Object> overAll = new HashMap<String, Object>();
		if(response.isEmpty()) {
			return null;
		} else {
			JsonParser jsonParser = new JsonParser();
			JsonArray jsonArray = null;
			//Map<String, Object> overAll = new HashMap<String, Object>();
			try {
				jsonArray = (JsonArray) jsonParser.parse(response);
				overAll = parseJsonArray(jsonArray);
			} catch (Exception e) {
				JsonObject object = (JsonObject) jsonParser.parse(response);
				Set<Entry<String, JsonElement>> entrySet = object.entrySet();
				for(Entry<String, JsonElement> entry : entrySet) {
					JsonElement element = entry.getValue();
					if(element.isJsonArray()) {
						overAll.put(entry.getKey(), parseJsonArray(element.getAsJsonArray()));
					} else if(element.isJsonPrimitive()){
						overAll.put(entry.getKey(), element);
					} else {
						overAll.put(entry.getKey(), parseJsonObject(element, overAll));
					}
				}
			}
			
			/*for (Map.Entry<String, Object> entry : overAll.entrySet()) {
			    System.out.println(entry.getKey() + "> " + entry.getValue());
			}*/
		}
		return overAll;
	}
	
	public static Map jsonToMap(String t)  {
	//	System.out.println();
        HashMap<String, String> map = new HashMap<String, String>();
        if(!t.startsWith("["))
        	t= "["+t+"]";
        JSONArray jarr=new JSONArray(t);
        Iterator<?> keys = jarr.getJSONObject(0).keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jarr.getJSONObject(0).get(key).toString(); 
            map.put(key, value);
        }
        return map;
    }
	private static Map<String, Object> parseJsonObject(JsonElement jsonElement, Map<String, Object> map) {
		Map<String, Object> mapInparseJsonArray = new HashMap<String, Object>();
		if(jsonElement.isJsonObject()) {
			JsonObject object = jsonElement.getAsJsonObject();
			Set<Entry<String, JsonElement>> entrySet = object.entrySet();
			for(Entry<String, JsonElement> entry : entrySet) {
				JsonElement element = entry.getValue();
				if(element.isJsonArray()) {
					map.put(entry.getKey(), parseJsonArray(element.getAsJsonArray()));
				} else {
					mapInparseJsonArray.put(entry.getKey(), entry.getValue());
				}
			}
		} else if(jsonElement.isJsonArray()) {
			return parseJsonArray(jsonElement.getAsJsonArray());
		}
		return mapInparseJsonArray;
	}
	
	private static Map<String, Object> parseJsonArray(JsonArray jsonArray) {
		Map<String, Object> mapInparseJsonArray = new HashMap<String, Object>();
		int index = 0;
		for(JsonElement jsonElement: jsonArray) {
			Map<String, Object> arrayMap = new HashMap<String, Object>();
			if(jsonElement.isJsonObject()) {
				JsonObject object = jsonElement.getAsJsonObject();
				Set<Entry<String, JsonElement>> entrySet = object.entrySet();
				for(Entry<String, JsonElement> entry : entrySet) {
					JsonElement element = entry.getValue();
					if(element.isJsonArray()) {
						arrayMap.put(entry.getKey(), parseJsonArray(element.getAsJsonArray()));
					} else {
						arrayMap.put(entry.getKey(), entry.getValue());
					}
				}
			}
			mapInparseJsonArray.put(index+"", arrayMap);
			index++;
		}
		return mapInparseJsonArray;
	}
}
